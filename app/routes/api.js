var express = require("express");
router = express.Router();
var chatCntr = require(__dirname+"/../controllers/chat");
var asigCntr = require(__dirname+"/../controllers/asignatura");

router.route("/chat").get(async(req,res,next)=>{
    var result = await chatCntr.load();
    res.status(200).jsonp(result);
});

router.route("/asignatura").get(async(req,res,next)=>{
    var result = await asigCntr.load();
    res.status(200).jsonp(result);
});

module.exports = router;