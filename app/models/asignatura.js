var mongoose = require("mongoose"),
    Schema = mongoose.Schema

    var asigSchema = new Schema({
    id:{type:[ObjectID]},
    Nombre: {type:string},
    Num_horas: {type:string},
    Docente: {type:[ObjectID]},
    Alumnos: {type:[ObjectID]}
    });

module.exports = mongoose.model("Asignatura",asigSchema);